# appBaseDatosScript

create database altioraCorpVentas
-- public.cliente definition

-- Drop table

-- DROP TABLE public.cliente;

CREATE TABLE public.cliente (
	id_cliente serial4 NOT NULL,
	apellido_cliente varchar(255) NULL,
	direccion_cliente varchar(255) NULL,
	estado_cliente bpchar(1) NULL,
	identificacion_cliente varchar(255) NULL,
	nombre_cliente varchar(255) NULL,
	telefono_cliente varchar(255) NULL,
	CONSTRAINT cliente_pkey PRIMARY KEY (id_cliente)
);
-- public.login definition

-- Drop table

-- DROP TABLE public.login;

CREATE TABLE public.login (
	idusuario serial4 NOT NULL,
	apellidousuario varchar(255) NULL,
	estado bpchar(1) NULL,
	nombreusuario varchar(255) NULL,
	"password" varchar(255) NULL,
	usuario varchar(255) NULL,
	CONSTRAINT login_pkey PRIMARY KEY (idusuario)
);
-- public.ordencabecera definition

-- Drop table

-- DROP TABLE public.ordencabecera;

CREATE TABLE public.ordencabecera (
	idorden serial4 NOT NULL,
	cedulacliente varchar(255) NULL,
	direccioncliente varchar(255) NULL,
	estado bpchar(1) NULL,
	fechacreacionorden varchar(255) NULL,
	id_cliente int4 NULL,
	nombrecliente varchar(255) NULL,
	CONSTRAINT ordencabecera_pkey PRIMARY KEY (idorden)
);
-- public.ordendetalle definition

-- Drop table

-- DROP TABLE public.ordendetalle;

CREATE TABLE public.ordendetalle (
	idordendetalle serial4 NOT NULL,
	cantidad int4 NULL,
	estado bpchar(1) NULL,
	idorden int4 NULL,
	idproducto int4 NULL,
	iva float8 NULL,
	total float8 NULL,
	CONSTRAINT ordendetalle_pkey PRIMARY KEY (idordendetalle)
);
-- public.producto definition

-- Drop table

-- DROP TABLE public.producto;

CREATE TABLE public.producto (
	idproducto serial4 NOT NULL,
	estado bpchar(1) NULL,
	nombreproducto varchar(255) NULL,
	precioventa float8 NULL,
	stock int4 NULL,
	CONSTRAINT producto_pkey PRIMARY KEY (idproducto)
);
